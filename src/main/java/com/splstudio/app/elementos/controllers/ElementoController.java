package com.splstudio.app.elementos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.splstudio.app.commons.models.entity.Elemento;
import com.splstudio.app.commons.models.entity.Proyecto;
import com.splstudio.app.commons.models.entity.TipoElemento;
import com.splstudio.app.elementos.models.service.IElementoService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ElementoController {
	
	@Autowired
	private IElementoService elementoService;
	
	
	@GetMapping("/listar/{idProyecto}")
	public List<Elemento> listar(@PathVariable Long idProyecto) {
		return elementoService.findByProyecto(idProyecto);
	}
	
	
	@GetMapping("/listarCompiacion/{idProyecto}/{idProducto}/{grupoPlantilla}")
	public List<Elemento> listarCompilacion(@PathVariable Long idProyecto, @PathVariable String idProducto,  @PathVariable String grupoPlantilla) {
		return elementoService.findBySelection(idProyecto, idProducto, grupoPlantilla);
	}
	
	@GetMapping("/cargar/{id}")
	public Elemento cargar(@PathVariable String id) {
		return elementoService.findById(id);
	}

	
	
	@PostMapping("/actualizar")
	@ResponseStatus(HttpStatus.CREATED)
	public Boolean update(@RequestBody String elementos) {
		try {
			return elementoService.crearOActualizar(elementos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	
}
