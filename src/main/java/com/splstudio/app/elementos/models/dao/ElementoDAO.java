package com.splstudio.app.elementos.models.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.splstudio.app.commons.models.entity.Elemento;

public interface ElementoDAO extends CrudRepository<Elemento, String>{
	List<Elemento> findByProyecto(Long idProyecto);
	List<Elemento> findBySelection(Long idProyecto, String idProducto, String grupoPlantilla);
}
