package com.splstudio.app.elementos.models.service;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.splstudio.app.commons.models.entity.Elemento;
import com.splstudio.app.commons.models.entity.Proyecto;
import com.splstudio.app.commons.models.entity.TipoElemento;
import com.splstudio.app.elementos.models.dao.ElementoDAO;

@Service
public class ElementoServiceImp implements IElementoService{
	
	@Autowired
	private ElementoDAO elementoDAO;
	


	@Override
	@Transactional(readOnly = true)
	public List<Elemento> findAll() {
		return (List<Elemento>) elementoDAO.findAll();
	}



	@Override
	public List<Elemento> findByProyecto(Long idProyecto) {
		return  elementoDAO.findByProyecto(idProyecto);
	}



	@Override
	public boolean crearOActualizar(String jsonInfo) throws Exception {


		try {
			//String jsonDecoded = URLDecoder.decode(jsonInfo, StandardCharsets.UTF_8.toString());
			JSONObject objetoInfo = new JSONObject(jsonInfo);
			JSONArray elementos = objetoInfo.getJSONArray("elementos");
		
			for(Object elementoObj : elementos) {
				
				
					JSONObject elemento = (JSONObject) elementoObj;
					
					String id = elemento.getString("id");
					Elemento el = null;
					
	
					//Se trata de un elemento ya presente
					try {
						el = elementoDAO.findById(id).get();
					}catch(Exception e){
						
					}
					if( el == null) {
						//El elemento no existe y se debe crear
						Long idProyecto = objetoInfo.getLong("idProyecto");
						//Se trata de un nuevo elemento
						el = new Elemento();
						el.setId(id);
						el.setIdProyecto(idProyecto);
						el.setCreateAt(new Date());
						el.setTipo(TipoElemento.values()[elemento.getInt("tipo")]);
						
					}
					
					
					if(el != null) {
						String payload =  getStringDeJson(elemento,"payload");
						if(payload != null) {
							el.setPayload(payload);
						}
						
						String nombre =  getStringDeJson(elemento,"nombre");
						if(nombre != null) {
							el.setNombre( nombre);
						}
						
						String grupo =  getStringDeJson(elemento,"grupo");
						if(grupo != null) {
							el.setGrupo(grupo);
						}
						
						
						
					}
									
					elementoDAO.save(el);

			}
			
			procesaEliminados(objetoInfo);
		
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
	
		return true;
	}

	
	private String getStringDeJson(JSONObject json, String prop) {
		try {
			
			return json.getString(prop);
		}catch(Exception e) {
			System.out.println("No se ha podido obtener " + prop);
		}
		
		return null;
	}
	
	
	private void procesaEliminados(JSONObject objetoInfo) throws Exception{
		ArrayList<String> elementosEliminados = new  ArrayList<String>();
		if(objetoInfo.has("eliminados")) {
			JSONArray elementosEliminar = objetoInfo.getJSONArray("eliminados");
			for(Object elementoObj : elementosEliminar) {
				String elemento = (String) elementoObj;
				if(elementoDAO.existsById(elemento)) {
					elementoDAO.deleteById(elemento);	
				}
			}
		}

	}

	@Override
	public Elemento findById(String id) {
		return elementoDAO.findById(id).get();
	}



	@Override
	public List<Elemento> findBySelection(Long idProyecto, String idProducto, String grupoPlantilla) {
		return elementoDAO.findBySelection(idProyecto, idProducto, grupoPlantilla);
	}

}
