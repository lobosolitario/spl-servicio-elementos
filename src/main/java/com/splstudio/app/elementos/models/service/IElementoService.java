package com.splstudio.app.elementos.models.service;

import java.util.List;

import com.splstudio.app.commons.models.entity.Elemento;
import com.splstudio.app.commons.models.entity.TipoElemento;


public interface IElementoService {
	//Interface del servicio
	
	public List<Elemento> findAll();
	public List<Elemento> findByProyecto(Long idProyecto);
	public List<Elemento> findBySelection(Long idProyecto, String idProducto, String grupoPlantilla);
	public Elemento findById(String id);
	public boolean crearOActualizar(String jsonInfo) throws Exception;
}
